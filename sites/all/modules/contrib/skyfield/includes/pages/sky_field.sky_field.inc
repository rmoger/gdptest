<?php

/**
 * Page callback.
 */
function sky_field_sky_field_page($node) {
  $content = drupal_get_form('sky_field_sky_field_page_form', $node);
  return $content;
}

/**
 * Page form.
 */
function sky_field_sky_field_page_form($form, &$form_state, $node) {
  // Get fields values.
  if (isset($form_state['values']['sky_field']['fields'])) {
    $sky_fields = $form_state['values']['sky_field']['fields'];
  }
  else {
    $sky_fields = sky_field_db_field_select(array('nid' => $node->nid), array('nid'));
  }
  
  // Populate status values.
  _sky_field_sky_field_page_populate_status_values($sky_fields);
  
  // Detect clicked button.
  $clicked_button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : FALSE;
  
  // Process clicked buttons.
  _sky_field_sky_field_page_process_add_field_button($clicked_button, $sky_fields);
  _sky_field_sky_field_page_process_remove_field_button($clicked_button, $sky_fields);
  
  // Add vars to $form_state.
  $form_state['node'] = $node;
  $form_state['wrapper_id'] = 'node-field-node-field-form';
  $form_state['sky_fields'] = $sky_fields;
  
  // Building node fields form.
  $form['sky_field'] = _sky_field_sky_field_page_fields_form($form_state, $sky_fields);
  
  // Return form with standard submit button and cancel link.
  $form_title = t('Edit fields for %node_name', array('%node_name' => $form_state['node']->title));
  $form_link = 'node/' . $form_state['node']->nid;
  $form = confirm_form($form, $form_title, $form_link, '', t('Save'), t('Cancel'));
  
  return $form;
}

/**
 * Default validate handler.
 */
function sky_field_sky_field_page_form_validate($form, &$form_state) {
  // Detect clicked button.
  $clicked_button = isset($form_state['clicked_button']) ? $form_state['clicked_button'] : FALSE;
  
  // Validate clicked buttons.
  _sky_field_sky_field_page_validate_save_button($clicked_button, $form, &$form_state);
}

/**
 * Default submit handler.
 */
function sky_field_sky_field_page_form_submit($form, &$form_state) {
  $sky_fields = &$form_state['values']['sky_field']['fields'];
  
  if (empty($sky_fields)) {
    return;
  }
  
  // Save fields changes.
  foreach ($sky_fields as &$sky_field) {
    if ($sky_field['removed']) {
      if (!$sky_field['new']) {
        $sky_field['op'] = 'delete';
        sky_field_db_field_delete($sky_field, array('id'));
      }
    }
    else if ($sky_field['new']) {
      $sky_field['op'] = 'insert';
      sky_field_db_field_insert($sky_field);
    }
    else {
      $sky_field['op'] = 'update';
      if (!empty($sky_field['no_need_updating'])) {
        continue;
      } 
      sky_field_db_field_update($sky_field, array('id'));
    }
  }
}

/**
 * Ajax handler.
 */
function sky_field_sky_field_page_js($form, $form_state) {
  return $form['sky_field'];
}

/**
 * Fields form.
 */
function _sky_field_sky_field_page_fields_form(&$form_state, $sky_fields) {
  // Building node fields form.
  $form = array(
    '#type' => 'container',
    '#attributes' => array('id' => $form_state['wrapper_id']),
    '#tree' => TRUE,
    'fields' => array(
      '#tree' => TRUE,
    ),
  );
  
  // "Add field" button.
  $form['add'] = array(
    '#weight' => 10,
    '#type' => 'button',
    '#name' => 'add',
    '#value' => t('Add field'),
    '#ajax' => array(
      'callback' => 'sky_field_sky_field_page_js',
      'wrapper' => $form_state['wrapper_id'],
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#attributes' => array('class' => array('button add')),
    '#sky_field_add_field' => TRUE,
  );
  
  // Building node field form.
  foreach ($sky_fields as $key => $sky_field) {
    $sky_field['key'] = $key;
    $form['fields'][] = _sky_field_sky_field_page_field_form(&$form_state, $sky_field);
  }
  
  return $form;
}

function _sky_field_sky_field_page_field_form(&$form_state, $sky_field) {
  // Default node field values.
  $default_sky_field = array(
    'title' => '',
    'show_title' => FALSE,
    'value' => '',
  );
  $sky_field += $default_sky_field;
  
  // Building node field form.
  $form = array(
    '#type' => 'fieldset',
    '#access' => !$sky_field['removed'],
    '#title' => $sky_field['new'] ? t('New field') : $sky_field['title'],
  );
  
  // Field values.
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $sky_field['title'],
    '#length' => 256,
  );
  $form['show_title'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show title'),
    '#title_display' => 'before',
    '#field_suffix' => 'Yes',
    '#default_value' => $sky_field['show_title'],
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Field type'),
    '#options' => sky_field_sky_field_types_options(),
    '#empty_option' => t('- Select value -'),
    '#empty_value' => 0,
    '#ajax' => array(
      'callback' => 'sky_field_sky_field_page_js',
      'wrapper' => $form_state['wrapper_id'],
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#default_value' => $sky_field['type'],
  );
  $form['value'] = sky_field_sky_field_widget_form($sky_field['type']);
  $form['value'] += array(
    '#title' => t('Value'),
    '#default_value' => $sky_field['value'],
  );
  
  // Hidden values.
  if (!$sky_field['new']) {
    $form['id'] = array(
      '#type' => 'value',
      '#value' => $sky_field['id'],
    );
  }
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $form_state['node']->nid,
  );
  
  // Status values.
  $form['new'] = array(
    '#type' => 'value',
    '#value' => $sky_field['new'],
  );
  $form['removed'] = array(
    '#type' => 'value',
    '#value' => $sky_field['removed'],
  );
  
  // "Remove field" button.
  $form['remove'] = array(
    '#type' => 'button',
    '#name' => 'remove_' . $sky_field['key'],
    '#value' => t('Remove item'),
    '#ajax' => array(
      'callback' => 'sky_field_sky_field_page_js',
      'wrapper' => $form_state['wrapper_id'],
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#attributes' => array('class' => array('button remove')),
    '#sky_field_remove_field' => $sky_field['key'],
  );
  
  return $form;
}

/**
 * Populate status values.
 */
function _sky_field_sky_field_page_populate_status_values(&$sky_fields) {
  foreach ($sky_fields as &$sky_field) {
    $sky_field += array(
      'new' => FALSE,
      'removed' => FALSE,
    );
  }
}

/**
 * Process "Add field" button. 
 */
function _sky_field_sky_field_page_process_add_field_button($clicked_button, &$sky_fields) {
  if (!isset($clicked_button['#sky_field_add_field'])) {
    return;
  }
  
  $sky_fields[] = array(
    'new' => TRUE,
    'removed' => FALSE,
  );
}

/**
 * Process "Remove field" button. 
 */
function _sky_field_sky_field_page_process_remove_field_button($clicked_button, &$sky_fields) {
  if (!isset($clicked_button['#sky_field_remove_field'])) {
    return;
  }
  
  $sky_fields[$clicked_button['#sky_field_remove_field']]['removed'] = TRUE;
}

/**
 * Validate "Save" button. 
 */
function _sky_field_sky_field_page_validate_save_button($clicked_button, $form, &$form_state) {
  if ($clicked_button['#array_parents'] != array('actions', 'submit')) {
    return;
  }
  
  $sky_fields = &$form_state['values']['sky_field']['fields'];
  
  if (empty($sky_fields)) {
    return;
  }
  
  foreach ($sky_fields as $key => $sky_field) {
    if (!empty($sky_field['no_need_validation'])) {
      continue;
    }
    if ($sky_field['removed']) {
      continue;
    }
    if (!$sky_field['title']) {
      form_set_error('sky_field][fields][' . $key . '][title', t('Enter title for field!'));
    }
    if (!$sky_field['type']) {
      form_set_error('sky_field][fields][' . $key . '][type', t('Select type of field!'));
    }
    else if (!$sky_field['value']) {
      form_set_error('sky_field][fields][' . $key . '][value', t('Enter value for field!'));
    }
  }
}

