<?php

define('SHADOW_VAR_CRON_INDEX_LIMIT', 'shadow_cron_index_limit');
define('SHADOW_VAR_CRON_INDEX_LIMIT_DEFAULT', 50);

define('SHADOW_REGEX_TABLENAME', '/^["]?[\\{]?[a-z][a-z0-9_]*[\\}]?["]?$/si');
define('SHADOW_REGEX_FIELDNAME', '/^["]?[a-z][a-z0-9_\\.]*["]?$/si');
define('SHADOW_REGEX_SCALAR', '/^(\'[^\\%]+\'|[0-9\\.]+)$/si');

/**
 * Implements hook_menu().
 */
function shadow_menu() {
  $menu = array();
  
  $menu['admin/structure/shadow'] = array(
    'title' => 'Shadow',
    'page callback' => 'shadow_tables_page',
    'access arguments' => array('administer site configuration'),
    'description' => 'Optimize SQL queries.',
    'type' => MENU_NORMAL_ITEM,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables'] = array(
    'title' => 'Tables',
    'page callback' => 'shadow_tables_page',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'shadow.admin.inc',
    'weight' => 1,
  );

  $menu['admin/structure/shadow/tables/%'] = array(
    'title' => 'Table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_table_actions_form', 4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables/%/rebuild'] = array(
    'title' => 'Rebuild table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_table_rebuild_form', 4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables/%/indexes'] = array(
    'title' => 'Edit indexes',
    'page callback' => 'shadow_indexes_page',
    'page arguments' => array(4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables/%/indexes/%/edit'] = array(
    'title' => 'Edit index',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_index_form', 4, 6),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables/%/indexes/%/delete'] = array(
    'title' => 'Delete index',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_delete_index_form', 4, 6),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/tables/%/delete'] = array(
    'title' => 'Delete table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_table_delete_form', 4),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/queries'] = array(
    'title' => 'Queries',
    'page callback' => 'shadow_queries_page',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'shadow.admin.inc',
    'weight' => 2,
  );

  $menu['admin/structure/shadow/queries/list'] = array(
    'title' => 'List',
    'page callback' => 'shadow_queries_page',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'file' => 'shadow.admin.inc',
    'weight' => 2,
  );

  $menu['admin/structure/shadow/queries/list/%'] = array(
    'title' => 'Query',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_choose_table_form', 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/queries/list/%/new'] = array(
    'title' => 'Create a new table',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_create_table_form', 5),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
    'file' => 'shadow.admin.inc',
  );

  $menu['admin/structure/shadow/queries/add'] = array(
    'title' => 'Add query',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_add_query_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'shadow.admin.inc',
    'weight' => 1,
  );

  $menu['admin/structure/shadow/test'] = array(
    'title' => 'Test query',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('shadow_queries_test_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'shadow.admin.inc',
    'weight' => 3,
  );

  return $menu;
}

/**
 * Speed up query.
 * 
 * This function can be used for both raw SQL codes and SelectQuery objects.
 * For SelectQuery objects, use the following syntax:
 * 
 * shadow(SelectQuery $query, $guid, $description);
 * 
 * Example:
 * 
 * $query = db_select('node', 'n')
 *     ->fields('n', array('nid', 'title'))
 *     ->condition('n.nid', 123);
 * $guid = 'query1';
 * shadow($query, $quid, 'Example query #1');
 * $results = $query->execute()->fetchAll();
 * 
 * Note that you must store guid in a variable, because only variables cna be
 * passed by reference. You may use shadow_object() directly to prevent that.
 * 
 * For raw SQL codes use the following syntax:
 * 
 * shadow($sql, $args, $guid, $description);
 * 
 * Example:
 * 
 * $sql = 'SELECT n.nid, n.title FROM {node} n WHERE n.nid = :nid';
 * $args = array(':nid' => 123);
 * shadow($sql, $args, 'query2', 'Example query #2');
 * $results = db_query($sql, $args)->fetchAll();
 * 
 * Always pass the arguments array to db_query, even if you do not use arguments
 * yourself. Shadow may add arguments.
 * 
 * @param mixed $query SelectQuery or raw SQL code.
 * @param mixed $a2 Arguments when using raw SQL code or guid when using an object.
 * @param string $a3 Guid when using raw SQL code or description when using an object.
 * @param string $a4 Description when using raw SQL code. Leave empty when using an object.
 */
function shadow(&$query, &$a2, $a3, $a4 = NULL) {
  if (is_object($query) && $query instanceof SelectQuery) {
    shadow_object($query, $a2, $a3);
  }
  elseif (is_string($query)) {
    shadow_sql($query, $a2, $a3, $a4);
  }
}

/**
 * Speed up raw SQL query.
 * 
 * Example:
 * 
 * $sql = 'SELECT n.nid, n.title FROM {node} n WHERE n.nid = :nid';
 * $args = array(':nid' => 123);
 * shadow_sql($sql, $args, 'query2', 'Example query #2');
 * $results = db_query($sql, $args)->fetchAll();
 * 
 * Always pass the arguments array to db_query, even if you do not use arguments
 * yourself. Shadow may add arguments.
 * 
 * @param string $sql
 * @param string $guid
 * @param string $description
 */
function shadow_sql(&$sql, &$args, $guid, $description) {
  $res = db_query('SELECT * FROM {shadow_query} WHERE guid = :guid', array(':guid' => $guid));
  
  if ($query = $res->fetch()) {
    $query->last_use = time();
    if ($sql != $query->query) {
      $query->query_changed = time();
      $query->query = $sql;
      drupal_write_record('shadow_query', $query, 'qid');
    }
    if ($query->tid) {
      _shadow_rewrite($sql, $args, $query->tid);
    }
  }
  else {
    $query = new stdClass();
    $query->guid = $guid;
    $query->description = $description;
    $query->query = $sql;
    $query->query_changed = time();
    $query->last_use = time();
    drupal_write_record('shadow_query', $query);
  }
}

/**
 * Speed up SelectQuery object.
 * 
 * Example:
 * 
 * $query = db_select('node', 'n')
 *     ->fields('n', array('nid', 'title'))
 *     ->condition('n.nid', 123);
 * shadow_object($query, 'query1', 'Example query #1');
 * $results = $query->execute()->fetchAll();
 * 
 * @param SelectQuery $query
 * @param string $guid
 * @param string $description 
 */
function shadow_object(SelectQuery &$query, $guid, $description) {
  _shadow_load_classes();
  $query = new ShadowSelectQuery($query, $guid, $description);
}

function _shadow_rewrite(&$sql, &$args, $tid) {
  _shadow_load_classes();

  $query = new ShadowQuery($sql);

  $filter_fields = array();
  $sort_fields = array();

  $table_sql = 'SELECT tid, db_name, base_table FROM {shadow_table} WHERE tid = :tid';
  $res = db_query($table_sql, array(':tid' => $tid));
  if (!$table = $res->fetch()) {
    return FALSE;
  }

  $pkey = _shadow_get_primary_key($table->base_table);
  $pkey_relations = array();

  $columns_sql = 'SELECT db_name, definition, invert_base
  FROM {shadow_column}
  WHERE tid = :tid';
  $res = db_query($columns_sql, array(':tid' => $tid));
  while ($column = $res->fetch()) {
    $definition = $column->definition . ($column->invert_base ? '*' : '');
    $fields[$definition] = $column->db_name;
    
    foreach ($pkey as $field) {
      if ($column->definition == $field) {
        $pkey_relations[$column->db_name] = str_replace($table->base_table . '.', '', $field);
      }
    }
  }

  if (count($pkey) != count($pkey_relations)) {
    return FALSE;
  }
  
  $query->shadow($table->db_name, $pkey_relations, $fields);

  $sql = (string) $query;
}

/**
 * Implements hook_theme().
 */
function shadow_theme($existing, $type, $theme, $path) {
  return array(
    'shadow_fields' => array(
      'render element' => 'element',
      'file' => 'shadow.admin.inc',
    ),
  );
}

/**
 * Implements hook_entity_insert().
 */
function shadow_entity_insert($entity, $type) {
  if (!$info = _shadow_storage_info($entity, $type)) {
    return;
  }
  module_load_include('inc', 'shadow', 'shadow.index');
  shadow_index('insert', $info->base_table, $info->pkey);
}

/**
 * Implements hook_entity_update().
 */
function shadow_entity_update($entity, $type) {
  if (!$info = _shadow_storage_info($entity, $type)) {
    return;
  }
  module_load_include('inc', 'shadow', 'shadow.index');
  shadow_index('update', $info->base_table, $info->pkey);
}

/**
 * Implements hook_entity_delete().
 */
function shadow_entity_delete($entity, $type) {
  if (!$info = _shadow_storage_info($entity, $type)) {
    return;
  }
  module_load_include('inc', 'shadow', 'shadow.index');
  shadow_index('delete', $info->base_table, $info->pkey);
}

/**
 * Implements hook_exit().
 *
 * Process the index queue.
 *
 * @see shadow_index()
 */
function shadow_exit($destination = NULL) {
  global $_shadow_index_queue;
  
  if (empty($_shadow_index_queue)) {
    return;
  }
  
  module_load_include('inc', 'shadow', 'shadow.index');
  foreach ($_shadow_index_queue as $item) {
    extract($item);
    shadow_index($op, $base_table, $pkey, 2);
  }
}

/**
 * Implements hook_cron().
 *
 * Process the index queue.
 *
 * @see shadow_index()
 */
function shadow_cron() {
  module_load_include('inc', 'shadow', 'shadow.index');
  $processed = array();
  $limit = variable_get(SHADOW_VAR_CRON_INDEX_LIMIT, SHADOW_VAR_CRON_INDEX_LIMIT_DEFAULT);
  $queue = db_select('shadow_index_queue', 'siq')
           ->fields('siq', array('iqid', 'op', 'base_table', 'pkey'))
           ->orderBy('siq.iqid')
           ->range(0, $limit)
           ->execute()
           ->fetchAll();
  foreach ($queue as $item) {
    shadow_index($item->op, $item->base_table, unserialize($item->pkey), 3);
    $processed[] = $item->iqid;
  }
  if ($processed) {
    db_delete('shadow_index_queue')
      ->condition('iqid', $processed)
      ->execute();
  }
}

/**
 * Private function to get the primary key of a table.
 * 
 * @param string $table
 * @param bool $include_table
 */
function _shadow_get_primary_key($table, $include_table = TRUE) {
  if (!$schema = drupal_get_schema($table)) {
    return FALSE;
  }
  if (empty($schema['primary key'])) {
    return array();
  }
  $fields = array();
  foreach ($schema['primary key'] as $field) {
    $fields[] = $include_table ? "$table.$field" : $field;
  }
  return $fields;
}

/**
 * Private function to get the storage information of an object
 * 
 * @param object $entity
 * @param string $type
 */
function _shadow_storage_info($entity, $type) {
  $info = entity_get_info($type);
  if (empty($info['base table']) || !is_object($entity)) {
    return FALSE;
  }
  $output = new stdClass();
  $output->base_table = $info['base table'];
  $output->pkey = array();
  $fields = _shadow_get_primary_key($info['base table'], FALSE);
  foreach ($fields as $field) {
    if (!isset($entity->$field) || !is_scalar($entity->$field)) {
      return FALSE;
    }
    $output->pkey[$field] = $entity->$field;
  }
  return $output;
}

/**
 * Private function to number new arguments.
 * 
 * @param bool $reset
 */
function _shadow_argument_name($reset = FALSE) {
  static $i = 0;
  if ($reset) {
    $i = 0;
  }
  else {
    ++$i;
    return ":shadow$i";
  }
}

/**
 * Private function to load all classes.
 */
function _shadow_load_classes() {
  module_load_include('inc', 'shadow', 'classes/state_machine');
  module_load_include('inc', 'shadow', 'classes/state_holder');
  module_load_include('inc', 'shadow', 'classes/filter');
  module_load_include('inc', 'shadow', 'classes/column');
  module_load_include('inc', 'shadow', 'classes/query');
  module_load_include('inc', 'shadow', 'classes/index');
  module_load_include('inc', 'shadow', 'classes/select');
}
