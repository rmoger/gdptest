<?php
/**
 * @file
 * c2.features.inc
 */

/**
 * Implements hook_node_info().
 */
function c2_node_info() {
  $items = array(
    'c2' => array(
      'name' => t('c2'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
