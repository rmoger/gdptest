<?php
/**
 * @file
 * country2.features.inc
 */

/**
 * Implements hook_node_info().
 */
function country2_node_info() {
  $items = array(
    'country2' => array(
      'name' => t('Country2'),
      'base' => 'node_content',
      'description' => t('Test Country'),
      'has_title' => '1',
      'title_label' => t('Country Name'),
      'help' => '',
    ),
  );
  return $items;
}
