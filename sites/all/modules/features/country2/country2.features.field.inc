<?php
/**
 * @file
 * country2.features.field.inc
 */

/**
 * Implements hook_field_default_fields().
 */
function country2_field_default_fields() {
  $fields = array();

  // Exported field: 'node-country2-body'.
  $fields['node-country2-body'] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '1',
      'deleted' => '0',
      'entity_types' => array(
        0 => 'node',
      ),
      'field_name' => 'body',
      'foreign keys' => array(
        'format' => array(
          'columns' => array(
            'format' => 'format',
          ),
          'table' => 'filter_format',
        ),
      ),
      'indexes' => array(
        'format' => array(
          0 => 'format',
        ),
      ),
      'locked' => '0',
      'module' => 'text',
      'settings' => array(
        'views_base_table' => 0,
      ),
      'translatable' => '0',
      'type' => 'text_with_summary',
    ),
    'field_instance' => array(
      'bundle' => 'country2',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '0',
        ),
        'teaser' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(
            'trim_length' => 600,
          ),
          'type' => 'text_summary_or_trimmed',
          'weight' => 0,
        ),
        'view' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '2',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'body',
      'label' => 'Description',
      'required' => 0,
      'settings' => array(
        'display_summary' => 1,
        'text_processing' => '1',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '20',
          'summary_rows' => 5,
        ),
        'type' => 'text_textarea_with_summary',
        'weight' => '-4',
      ),
    ),
  );



for ($i = 1; $i <= 25; $i++) {



  // Exported field: 'node-country2-field_observation_field_1'.
  $fields['node-country2-field_observation_field_'  . $i] = array(
    'field_config' => array(
      'active' => '1',
      'cardinality' => '-1',
      'deleted' => '0',
      'entity_types' => array(),
      'field_name' => 'field_observation_field_'  . $i,
      'foreign keys' => array(),
      'indexes' => array(
        'date' => array(
          0 => 'date',
        ),
        'name' => array(
          0 => 'name',
        ),
        'observer' => array(
          0 => 'observer',
        ),
      ),
      'locked' => '0',
      'module' => 'gdp_field',
      'settings' => array(
        'field_suppress' => 'never',
        'field_suppress_blank' => FALSE,
        'views_base_table' => TRUE,
      ),
      'translatable' => '0',
      'type' => 'gdp_field_ob',
    ),
    'field_instance' => array(
      'bundle' => 'country2',
      'default_value' => NULL,
      'deleted' => '0',
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'gdp_field',
          'settings' => array(),
          'type' => 'gdp_field_ob_formatter',
          'weight' => '1',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'view' => array(
          'label' => 'above',
          'module' => 'gdp_field',
          'settings' => array(),
          'type' => 'gdp_field_ob_formatter',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_observation_field_'  . $i,
      'label' => 'Observation Field ' . $i,
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 0,
        'module' => 'gdp_field',
        'settings' => array(),
        'type' => 'gdp_field_ob_widget',
        'weight' => '-3',
      ),
    ),
  );

}



  // Translatables
  // Included for use with string extractors like potx.
  t('Description');


  return $fields;
}
