<?php
/**
 * @file
 * country2.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function country2_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_viewgroup|node|country2|view';
  $field_group->group_name = 'group_viewgroup';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'country2';
  $field_group->mode = 'view';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'viewgroup',
    'weight' => '0',
    'children' => array(
      0 => 'field_observation_field_1',
    ),
    'format_type' => 'view',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'primary_field' => '',
        'field_suppress' => 'never',
      ),
    ),
  );
  $export['group_viewgroup|node|country2|view'] = $field_group;

  return $export;
}
